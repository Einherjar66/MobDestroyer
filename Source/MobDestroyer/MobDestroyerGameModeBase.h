// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MobDestroyerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MOBDESTROYER_API AMobDestroyerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
