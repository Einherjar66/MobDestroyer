// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerCharacter.h"

#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"

#include "Components/CapsuleComponent.h"
#include "Components/BoxComponent.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "Enemy.h"
#include "Sound/Soundcue.h"
#include "Camera/CameraComponent.h"
#include "Math/NumericLimits.h"
#include "Animation/AnimInstance.h"

#include "MainPlayerController.h"

// Sets default values
AMainPlayerCharacter::AMainPlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmLength = 600.f;
	CameraSprinArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSprinArm"));
	CameraSprinArm->SetupAttachment(GetRootComponent());
	CameraSprinArm->TargetArmLength = SpringArmLength;
	CameraSprinArm->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraSprinArm, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	// Create SwordCollisionBox
	SwordCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("SwordCollisionBox"));
	SwordCollisionBox->SetupAttachment(GetMesh(), FName("SwordCollisionBox"));

	SwordCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SwordCollisionBox->SetCollisionObjectType(ECC_WorldDynamic);
	SwordCollisionBox->SetCollisionResponseToAllChannels(ECR_Ignore);
	SwordCollisionBox->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);

	// set Size for the Collision capsule
	GetCapsuleComponent()->SetCapsuleSize(48.f, 105.f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;			// Character moves in the direction of input...
	GetCharacterMovement()->RotationRate = FRotator(0.f, 540.f, 0.f);	// ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 650.f;						// Jump Hight
	GetCharacterMovement()->AirControl = 0.2f;

	// Init Enums
	MovementStatus = EMovementStatus::EMS_Jog;
	StaminaStatus = EStaminaStatus::ESS_Normal;

	JogSpeed = 650.f;
	SprintSpeed = 950.f;
	StaminaDrainRate = 25.f;
	MinSprintStamina = 15.f;

	MaxHealth = 100.f;
	Health = 55.f;
	MaxStamina = 90.f;
	Stamina = 45.f;
	MaxHonor = TNumericLimits<int32>::Max();
	Honor = 0;
	bHasCombatTarget = false;

	MontageSection = 0;

	bMovingForward = false;
	bMovingRight = false;
}
// Called when the game starts or when spawned
void AMainPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	SwordCollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AMainPlayerCharacter::SwordBoxBeginOverlap);

	MainPlayerController = Cast<AMainPlayerController>(GetController());
	// 	if (MainPlayerController)
	// 	{
	// 		MainPlayerController->GameModeOnly();
	// 	}

}

// Called every frame
void AMainPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (MovementStatus == EMovementStatus::EMS_Dead) return;

	float DeltaStamina = StaminaDrainRate * DeltaTime;
	switch (StaminaStatus)
	{
	case EStaminaStatus::ESS_Normal:
		if (bShiftDown)
		{
			if (Stamina - DeltaStamina <= MinSprintStamina)
			{
				SetStaminaStatus(EStaminaStatus::ESS_BelowMinimum);
				Stamina -= DeltaStamina;
			}
			else
			{
				Stamina -= DeltaStamina;
			}
			if (bMovingForward || bMovingRight)
			{
				SetMovementStatus(EMovementStatus::EMS_Sprinting);
			}
			else
			{
				SetMovementStatus(EMovementStatus::EMS_Jog);
			}
		}
		else // Shift key up
		{
			if (Stamina + DeltaStamina >= MaxStamina)
			{
				Stamina = MaxStamina;
			}
			else
			{
				Stamina += DeltaStamina;
			}
			SetStaminaStatus(EStaminaStatus::ESS_Normal);
		}
		break;

	case EStaminaStatus::ESS_BelowMinimum:
		if (bShiftDown)
		{
			if (Stamina - DeltaStamina <= 0.f)
			{
				SetStaminaStatus(EStaminaStatus::ESS_Exhausted);
				Stamina = 0;
				SetMovementStatus(EMovementStatus::EMS_Jog);
			}
			else
			{
				Stamina -= DeltaStamina;
				if (bMovingForward || bMovingRight)
				{
					SetMovementStatus(EMovementStatus::EMS_Sprinting);
				}
				else
				{
					SetMovementStatus(EMovementStatus::EMS_Jog);
				}
			}
		}
		else// Shift key up
		{
			if (Stamina + DeltaStamina >= MinSprintStamina)
			{
				SetStaminaStatus(EStaminaStatus::ESS_Normal);
				Stamina += DeltaStamina;
			}
			else
			{
				Stamina += DeltaStamina;
			}
			SetMovementStatus(EMovementStatus::EMS_Jog);
		}
		break;

	case EStaminaStatus::ESS_Exhausted:
		if (bShiftDown)
		{
			Stamina = 0.f;
		}
		else // Shift key up
		{
			SetStaminaStatus(EStaminaStatus::ESS_ExhaustedRecovering);
			Stamina += DeltaStamina;
		}
		SetMovementStatus(EMovementStatus::EMS_Jog);
		break;

	case EStaminaStatus::ESS_ExhaustedRecovering:
		if (Stamina + DeltaStamina >= MinSprintStamina)
		{
			SetStaminaStatus(EStaminaStatus::ESS_Normal);
			Stamina += DeltaStamina;
		}
		else// Shift key up
		{
			Stamina += DeltaStamina;
		}
		SetMovementStatus(EMovementStatus::EMS_Jog);
		break;

	default:
		break;
	}
}

// Called to bind functionality to input
void AMainPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent)

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMainPlayerCharacter::ShiftDown);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMainPlayerCharacter::ShiftUp);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMainPlayerCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("LMB", IE_Pressed, this, &AMainPlayerCharacter::LMBDown);
	PlayerInputComponent->BindAction("LMB", IE_Released, this, &AMainPlayerCharacter::LMBUp);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMainPlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainPlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &AMainPlayerCharacter::LookUp);
	PlayerInputComponent->BindAxis("Turn", this, &AMainPlayerCharacter::Turn);
}

bool AMainPlayerCharacter::AllowInput(float Value)
{
	return(Controller != nullptr) &&
		(Value != 0.f) &&
		(!bAttacking) &&
		(MovementStatus != EMovementStatus::EMS_Dead);
}

void AMainPlayerCharacter::MoveForward(float Value)
{
	bMovingForward = false;

	if (AllowInput(Value))
	{
		const FRotator Rotation = Controller -> GetControlRotation();
		const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		AddMovementInput(Direction, Value);
		bMovingForward = true;
	}
}

void AMainPlayerCharacter::MoveRight(float Value)
{
	bMovingRight = false;
	if (AllowInput(Value))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);

		bMovingRight = true;
	}
}

void AMainPlayerCharacter::LookUp(float Value)
{
	if (AllowInput(Value))
	{
		AddControllerPitchInput(Value);
	}
}

void AMainPlayerCharacter::Turn(float Value)
{
	if (AllowInput(Value))
	{
		AddControllerYawInput(Value);
	}
}

void AMainPlayerCharacter::LMBDown()
{
	bLMBDown = true;
	if (MovementStatus == EMovementStatus::EMS_Dead) return;
	Attack();
}

void AMainPlayerCharacter::LMBUp()
{
	bLMBDown = false;
}

void AMainPlayerCharacter::ShiftDown()
{
	bShiftDown = true;
}

void AMainPlayerCharacter::ShiftUp()
{
	bShiftDown = false;
}

void AMainPlayerCharacter::Jump()
{
	if (MovementStatus != EMovementStatus::EMS_Dead)
	{
	
		Super::Jump();
	}
}

void AMainPlayerCharacter::PlayAttackSound()
{
	UGameplayStatics::PlaySound2D(this, AttackSound);
}

void AMainPlayerCharacter::PlayHitSound()
{
	UGameplayStatics::PlaySound2D(this, HitSound);
}

void AMainPlayerCharacter::Attack()
{
	
	if (!bAttacking && MovementStatus != EMovementStatus::EMS_Dead)
	{
		bAttacking = true;

		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance && CombatMontage)
		{
			switch (MontageSection)
			{
			case 0:
				AnimInstance->Montage_Play(CombatMontage, 1.3f);
				AnimInstance->Montage_JumpToSection(FName("Attack_Primary_A"), CombatMontage);
				MontageSection++;
				break;
			case 1:
				AnimInstance->Montage_Play(CombatMontage, 1.4f);
				AnimInstance->Montage_JumpToSection(FName("Attack_Primary_B"), CombatMontage);
				MontageSection = 0;
				break;
			default:
				break;
			}
		}
	}
}


void AMainPlayerCharacter::SwordBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

float AMainPlayerCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (Health - DamageAmount <= 0.f)
	{
		Health -= DamageAmount;
		Die();
		if (DamageCauser)
		{
			AEnemy* Enemy = Cast<AEnemy>(DamageCauser);
			if (Enemy)
			{
/*				Enemy->bHasValidTarget = false;*/
			}
		}
	}
	else
	{
		Health -= DamageAmount;
	}

	return DamageAmount;
}
void AMainPlayerCharacter::UpdateCombatTarget()
{
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors, EnemyFilter);

	if (OverlappingActors.Num() == 0)
	{
		if (MainPlayerController)
		{
			MainPlayerController->RemoveEnemyHealthBar();
		}
		return;
	}

	AEnemy* ClosestEnemy = Cast<AEnemy>(OverlappingActors[0]);
	if (ClosestEnemy)
	{
		FVector Location = GetActorLocation();
		float MinDistance = (ClosestEnemy->GetActorLocation() - Location).Size();

		for (auto Actor : OverlappingActors)
		{
			AEnemy* Enemy = Cast<AEnemy>(Actor);
			if (Enemy)
			{
				float DistanceToActor = (Enemy->GetActorLocation() - Location).Size();
				if (DistanceToActor < MinDistance)
				{
					MinDistance = DistanceToActor;
					ClosestEnemy = Enemy;
				}
			}
		}
		if (MainPlayerController)
		{
			MainPlayerController->DisplayEnemyHealthBar();
		}
		SetCombatTarget(ClosestEnemy);
		bHasCombatTarget = true;
	}
}

void AMainPlayerCharacter::IncrementHealth(float Amount)
{

	if (Health + Amount >= MaxHealth)
	{
		Health = MaxHealth;
	} 
	else
	{
		Health += Amount;
		UE_LOG(LogTemp,Warning,TEXT("%i"),Health);
	}
}


void AMainPlayerCharacter::IncrementStamina(float Amount)
{
	if (Stamina + Amount >= MaxStamina)
	{
		Stamina = MaxStamina;
	} 

	else
	{
		Stamina += Amount;
	}
}

void AMainPlayerCharacter::DecrementHealth(float Amount)
{

}

void AMainPlayerCharacter::IncrementHonor(int32 Amount)
{
	Honor += Amount;
}


void AMainPlayerCharacter::ActivateCollision()
{
	SwordCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}


void AMainPlayerCharacter::DeactivateCollision()
{
	SwordCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AMainPlayerCharacter::AttackEnd()
{
	bAttacking = false;
	
	if (bLMBDown)
	{
		Attack();
	}
}

void AMainPlayerCharacter::Die()
{
	if (MovementStatus == EMovementStatus::EMS_Dead) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && CombatMontage)
	{
		AnimInstance->Montage_Play(CombatMontage);
		AnimInstance->Montage_JumpToSection(FName("Death"));
	}
	SetMovementStatus(EMovementStatus::EMS_Dead);
}

void AMainPlayerCharacter::DeathEnd()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;
}

void AMainPlayerCharacter::SetMovementStatus(EMovementStatus Status)
{
	MovementStatus = Status;

	if (MovementStatus == EMovementStatus::EMS_Sprinting)
	{
		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	} 
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = JogSpeed;
	}
}

FRotator AMainPlayerCharacter::GetLookAtRotationYaw(FVector Target)
{
	FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Target);
	FRotator LookAtRotationYaw(0.f, LookAtRotation.Yaw, 0.f);
	return LookAtRotationYaw;
}

