// Copyright Epic Games, Inc. All Rights Reserved.

#include "MobDestroyer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MobDestroyer, "MobDestroyer" );
